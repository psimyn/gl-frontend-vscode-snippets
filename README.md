WIP

## Goal
Create snippets for components, store and spec files in order to decrease time 
spent in boilerplate

### Snippets list

1. Vue Component
2. Vue Mount point
3. Vuex actions
4. Vuex store index
5. Vuex mutations
3. Spec for vue component
4. Spec for vue component with axios mock adapter
5. Spec for vuex actions
6. Spec for vuex mutations
